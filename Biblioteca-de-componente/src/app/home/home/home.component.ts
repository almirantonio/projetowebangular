import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images: any[];
  constructor() { }

  ngOnInit() {
    this.images = [];
        this.images.push({source:'./assets/fazenda.jpg', alt:'Description for Image 1', title:'Title 1'});
        this.images.push({source:'./assets/aves_.jpg', alt:'Description for Image 2', title:'Title 2'});
        this.images.push({source:'./assets/vacas.jpg', alt:'Description for Image 3', title:'Title 3'});
        
  }

}
