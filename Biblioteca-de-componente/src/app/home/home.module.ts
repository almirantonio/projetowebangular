import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import {GalleriaModule} from 'primeng/galleria';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    GalleriaModule,
    RouterModule
   
   
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
