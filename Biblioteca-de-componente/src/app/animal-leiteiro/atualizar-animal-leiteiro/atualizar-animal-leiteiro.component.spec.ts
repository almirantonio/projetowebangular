import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAnimalLeiteiroComponent } from './atualizar-animal-leiteiro.component';

describe('AtualizarAnimalLeiteiroComponent', () => {
  let component: AtualizarAnimalLeiteiroComponent;
  let fixture: ComponentFixture<AtualizarAnimalLeiteiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarAnimalLeiteiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAnimalLeiteiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
