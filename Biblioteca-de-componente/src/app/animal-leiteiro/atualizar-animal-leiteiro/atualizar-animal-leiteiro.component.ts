import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnimalLeiteiroService } from '../AnimalLeiteiro.service';

@Component({
  selector: 'app-atualizar-animal-leiteiro',
  templateUrl: './atualizar-animal-leiteiro.component.html',
  styleUrls: ['./atualizar-animal-leiteiro.component.css']
})
export class AtualizarAnimalLeiteiroComponent implements OnInit {
  data_cio: Date;
  data_paricao: Date;
  id: number;
  @Input()  animalleiteiro: Array<any>;
  Data:boolean=false;
    constructor(private router: ActivatedRoute,
      private animalLeiteiroService: AnimalLeiteiroService) { 
      this.id= this.router.snapshot.params['id'];
      
    }
  
    ngOnInit() {
      this.listaId(this.id);
     
    }
    listaId(id: number){
     
      this.animalLeiteiroService.listarId(id).subscribe(dados => this.animalleiteiro = dados);
  
      
    }
  
    
    atualizar(animalleiteiro: any) {
      animalleiteiro.data_cio= this.data_cio.toLocaleDateString();
      animalleiteiro.data_paricao= this.data_paricao.toLocaleDateString();
      
  
      console.log(animalleiteiro);
   this.animalLeiteiroService.alterar(animalleiteiro).subscribe(() => {
       console.log('Atualizado com sucesso');
      
     
      }
      );
      }
  
      verificaValidTouched(campo){
        return !campo.valid && campo.touched;
      }
    aplicaCssErro(campo){
      return {
        'has-error': this.verificaValidTouched(campo),
        'has-feedback': this.verificaValidTouched(campo)
       
      }
    }

}
