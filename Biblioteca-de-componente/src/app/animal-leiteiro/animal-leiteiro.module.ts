import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastrarAnimalLeiteiroComponent } from './cadastrar-animal-leiteiro/cadastrar-animal-leiteiro.component';
import { ListarAnimalLeiteiroComponent } from './listar-animal-leiteiro/listar-animal-leiteiro.component';
import { AtualizarAnimalLeiteiroComponent } from './atualizar-animal-leiteiro/atualizar-animal-leiteiro.component';
import { CalendarModule } from 'primeng/calendar';
import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AnimalLeiteiroService } from './AnimalLeiteiro.service';
import { AppCampoControlErroComponent } from './app-campo-control-erro/app-campo-control-erro.component';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    TableModule,
    RouterModule,
    FormsModule
  ],
  declarations: [CadastrarAnimalLeiteiroComponent, ListarAnimalLeiteiroComponent, AtualizarAnimalLeiteiroComponent,AppCampoControlErroComponent],
  providers: [ AnimalLeiteiroService]

})
export class AnimalLeiteiroModule { }
