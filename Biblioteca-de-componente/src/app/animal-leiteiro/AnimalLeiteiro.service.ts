import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AnimalLeiteiroService {

  

  constructor(private http: HttpClient) { }
  
  listar(){
  return this.http.get<any[]>('http://localhost:8080/animal/leiteiro');
  }
  listarId(id : number){
    return this.http.get<any[]>(`http://localhost:8080/animal/leiteiro/${id}`);
    }
  
  adicionar(AnimalLeiteiro: any): Observable<any>{
    console.log(JSON.stringify(AnimalLeiteiro));
    return this.http.post('http://localhost:8080/animal/leiteiro',AnimalLeiteiro);
  }
  excluir(id: number){
    return this.http.delete(`http://localhost:8080/animal/leiteiro/${id}`);
  }
 
  alterar(AnimalLeiteiro: any): Observable<any> {
    return this.http.put(`http://localhost:8080/animal/leiteiro/${AnimalLeiteiro.id}`,
    AnimalLeiteiro);
    }
}
