import { AnimalLeiteiroService } from './../AnimalLeiteiro.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cadastrar-animal-leiteiro',
  templateUrl: './cadastrar-animal-leiteiro.component.html',
  styleUrls: ['./cadastrar-animal-leiteiro.component.css']
})
export class CadastrarAnimalLeiteiroComponent implements OnInit {
  data_cio: Date;
  data_paricao: Date;
  animalleiteiro: any;
  constructor( private animalLeiteiroService: AnimalLeiteiroService) { }

  ngOnInit() {
    this.animalleiteiro={};
  }
  onSubmit(from: FormGroup){
    
    this.animalleiteiro.data_cio= this.data_cio.toLocaleDateString();
    this.animalleiteiro.data_paricao= this.data_paricao.toLocaleDateString();
    
     console.log(this.animalleiteiro);
    
    
     this.animalLeiteiroService.adicionar(this.animalleiteiro)
     .subscribe(response => this.animalLeiteiroService.listar()
      )
   
   }
 
 
 
   verificaValidTouched(campo){
     return !campo.valid && campo.touched;
   }
 
   aplicaCssErro(campo){
     return {
       'has-error': this.verificaValidTouched(campo),
       'has-feedback': this.verificaValidTouched(campo)
      
     }
   }
 
}
