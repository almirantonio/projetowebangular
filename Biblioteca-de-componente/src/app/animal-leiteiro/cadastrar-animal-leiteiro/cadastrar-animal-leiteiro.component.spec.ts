import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarAnimalLeiteiroComponent } from './cadastrar-animal-leiteiro.component';

describe('CadastrarAnimalLeiteiroComponent', () => {
  let component: CadastrarAnimalLeiteiroComponent;
  let fixture: ComponentFixture<CadastrarAnimalLeiteiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarAnimalLeiteiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarAnimalLeiteiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
