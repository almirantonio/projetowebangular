import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAnimalLeiteiroComponent } from './listar-animal-leiteiro.component';

describe('ListarAnimalLeiteiroComponent', () => {
  let component: ListarAnimalLeiteiroComponent;
  let fixture: ComponentFixture<ListarAnimalLeiteiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAnimalLeiteiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAnimalLeiteiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
