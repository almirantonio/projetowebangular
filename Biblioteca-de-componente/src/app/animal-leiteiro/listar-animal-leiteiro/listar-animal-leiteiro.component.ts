import { AnimalLeiteiroService } from './../AnimalLeiteiro.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listar-animal-leiteiro',
  templateUrl: './listar-animal-leiteiro.component.html',
  styleUrls: ['./listar-animal-leiteiro.component.css']
})
export class ListarAnimalLeiteiroComponent implements OnInit {
  animalleiteiro: Array<any>;
  constructor(private animalLeiteiroService: AnimalLeiteiroService) {



   }

  ngOnInit() {
    this.listar();
  }
listar(){
    this.animalLeiteiroService.listar().subscribe(dados => this.animalleiteiro = dados);
  
    }
   
    excluir(id: number){
      this.animalLeiteiroService.excluir(id).subscribe(()=>{
        console.log('Escluido com sucesso');
        this.listar();
        
      });
    }
}
