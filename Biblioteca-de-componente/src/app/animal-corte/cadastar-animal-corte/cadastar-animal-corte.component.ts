import { AnimalCorteService } from './../AnimalCorte.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cadastar-animal-corte',
  templateUrl: './cadastar-animal-corte.component.html',
  styleUrls: ['./cadastar-animal-corte.component.css']
})
export class CadastarAnimalCorteComponent implements OnInit {
  data_nascimento: Date;
  animalCorte: any;
  constructor(private animalCorteService: AnimalCorteService) { }

  ngOnInit() {
    this.animalCorte={};
  }
  onSubmit(from: FormGroup){
   
    this.animalCorte.data_nascimento= this.data_nascimento.toLocaleDateString();
    
     console.log(this.animalCorte);
    
    
     this.animalCorteService.adicionar(this.animalCorte)
     .subscribe(response => this.animalCorteService.listar()
    
    
     )
     
   }
 
 
 
   verificaValidTouched(campo){
     return !campo.valid && campo.touched;
   }
 
   aplicaCssErro(campo){
     return {
       'has-error': this.verificaValidTouched(campo),
       'has-feedback': this.verificaValidTouched(campo)
      
     }
   }
 
  
   
 }
 

