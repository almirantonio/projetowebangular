import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastarAnimalCorteComponent } from './cadastar-animal-corte.component';

describe('CadastarAnimalCorteComponent', () => {
  let component: CadastarAnimalCorteComponent;
  let fixture: ComponentFixture<CadastarAnimalCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastarAnimalCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastarAnimalCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
