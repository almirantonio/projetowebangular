import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAnimalCorteComponent } from './atualizar-animal-corte.component';

describe('AtualizarAnimalCorteComponent', () => {
  let component: AtualizarAnimalCorteComponent;
  let fixture: ComponentFixture<AtualizarAnimalCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarAnimalCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAnimalCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
