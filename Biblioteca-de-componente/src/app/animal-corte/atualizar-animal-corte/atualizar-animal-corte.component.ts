import { AnimalCorteService } from './../AnimalCorte.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-atualizar-animal-corte',
  templateUrl: './atualizar-animal-corte.component.html',
  styleUrls: ['./atualizar-animal-corte.component.css']
})
export class AtualizarAnimalCorteComponent implements OnInit {
  data_nascimento: Date;
  id: number;
  @Input()  animalCorte: Array<any>;
  constructor(private router: ActivatedRoute,
    private animalCorteService: AnimalCorteService) { 
    this.id= this.router.snapshot.params['id']; }

  ngOnInit() {
    this.listaId(this.id);
  }

  listaId(id: number){
   
    this.animalCorteService.listarId(id).subscribe(dados => this.animalCorte = dados);

    
  }

  
  atualizar(animalCorte: any) {
    
    animalCorte.data_nascimento= this.data_nascimento.toLocaleDateString();

    console.log(animalCorte);
 this.animalCorteService.alterar(animalCorte).subscribe(() => {
     console.log('Atualizado com sucesso');
    
   
    }
    );
    }
  verificaValidTouched(campo){
    return !campo.valid && campo.touched;
  }

  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }
}
