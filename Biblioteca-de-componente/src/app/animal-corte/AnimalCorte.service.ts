import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AnimalCorteService {

  

  constructor(private http: HttpClient) { }
  
  listar(){
  return this.http.get<any[]>('http://localhost:8080/animal/corte');
  }
  listarId(id : number){
    return this.http.get<any[]>(`http://localhost:8080/animal/corte/${id}`);
    }
  
  adicionar(animalcorte: any): Observable<any>{
    console.log(JSON.stringify(animalcorte));
    return this.http.post('http://localhost:8080/animal/corte',animalcorte);
  }
  excluir(id: number){
    return this.http.delete(`http://localhost:8080/animal/corte/${id}`);
  }
 
  alterar(animalcorte: any): Observable<any> {
    return this.http.put(`http://localhost:8080/animal/corte/${animalcorte.id}`,
    animalcorte);
    }
}
