import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastarAnimalCorteComponent } from './cadastar-animal-corte/cadastar-animal-corte.component';
import { ListarAnimalCorteComponent } from './listar-animal-corte/listar-animal-corte.component';
import { AtualizarAnimalCorteComponent } from './atualizar-animal-corte/atualizar-animal-corte.component';
import { AnimalCorteService } from './AnimalCorte.service';
import { CalendarModule } from 'primeng/calendar';
import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppCampoControlErroComponent } from './app-campo-control-erro/app-campo-control-erro.component';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    TableModule,
    RouterModule,
    FormsModule
  ],
  declarations: [CadastarAnimalCorteComponent, ListarAnimalCorteComponent, AtualizarAnimalCorteComponent,AppCampoControlErroComponent],
  providers: [ AnimalCorteService]
})
export class AnimalCorteModule { }
