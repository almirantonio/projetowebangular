import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarAnimalCorteComponent } from './listar-animal-corte.component';

describe('ListarAnimalCorteComponent', () => {
  let component: ListarAnimalCorteComponent;
  let fixture: ComponentFixture<ListarAnimalCorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarAnimalCorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarAnimalCorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
