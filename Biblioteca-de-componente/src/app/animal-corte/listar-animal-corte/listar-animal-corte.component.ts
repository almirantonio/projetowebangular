import { AnimalCorteService } from './../AnimalCorte.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listar-animal-corte',
  templateUrl: './listar-animal-corte.component.html',
  styleUrls: ['./listar-animal-corte.component.css']
})
export class ListarAnimalCorteComponent implements OnInit {
  
  animalCorte: Array<any>;

  constructor(private animalCorteService: AnimalCorteService) { }

  ngOnInit() {
    this.listar();
  }
  listar(){
    this.animalCorteService.listar().subscribe(dados => this.animalCorte = dados);
  
    }
   
    excluir(id: number){
      this.animalCorteService.excluir(id).subscribe(()=>{
        console.log('Escluido com sucesso');
        this.listar();
        
      });
    }

}
