import { AuthGuard } from './guards/auth.guard';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { HomeComponent } from './home/home/home.component';
import { LoginComponent } from "./login/login/login.component";
import { FormularioFunciorarioComponent } from './funcionario/formulario-funciorario/formulario-funciorario.component';
import { TabelaFuncionarioComponent } from './funcionario/tabela-funcionario/tabela-funcionario.component';
import { AtualizarComponent } from './funcionario/atualizar/atualizar.component';
import { CadastrarRacaoComponent } from './racao/cadastrar-racao/cadastrar-racao.component';
import { ListarRacaoComponent } from './racao/listar-racao/listar-racao.component';
import { AtualizarRacaoComponent } from './racao/atualizar-racao/atualizar-racao.component';
import { CadastrarAvesComponent } from './aves/cadastrar-aves/cadastrar-aves.component';
import { AtualizarAvesComponent } from './aves/atualizar-aves/atualizar-aves.component';
import { ListarAvesComponent } from './aves/listar-aves/listar-aves.component';
import { CadastarAnimalCorteComponent } from './animal-corte/cadastar-animal-corte/cadastar-animal-corte.component';
import { ListarAnimalCorteComponent } from './animal-corte/listar-animal-corte/listar-animal-corte.component';
import { AtualizarAnimalCorteComponent } from './animal-corte/atualizar-animal-corte/atualizar-animal-corte.component';
import { CadastrarAnimalLeiteiroComponent } from './animal-leiteiro/cadastrar-animal-leiteiro/cadastrar-animal-leiteiro.component';
import { ListarAnimalLeiteiroComponent } from './animal-leiteiro/listar-animal-leiteiro/listar-animal-leiteiro.component';
import { AtualizarAnimalLeiteiroComponent } from './animal-leiteiro/atualizar-animal-leiteiro/atualizar-animal-leiteiro.component';

const appRoutes: Routes = [
    
  
    { path: 'login', component: LoginComponent },
    //rotas Funcionario
    { path: 'atualizar/:id', component: AtualizarComponent },
    { path: 'Formulario/Funcionaio', component: FormularioFunciorarioComponent,
     canActivate:[AuthGuard]
    },
    { path: 'Tabela/Funcionaio', component: TabelaFuncionarioComponent,
   canActivate:[AuthGuard]
     },
     
     //rotas Racao
     { path: 'atualizarRacao/:id', component: AtualizarRacaoComponent },
     { path: 'Formulario/Racao', component: CadastrarRacaoComponent,
       canActivate:[AuthGuard]
     },
     { path: 'Tabela/Racao', component: ListarRacaoComponent,
     canActivate:[AuthGuard]
      },

 //rotas aves
 { path: 'atualizarAves/:id', component: AtualizarAvesComponent },
 { path: 'Formulario/aves', component: CadastrarAvesComponent,
   canActivate:[AuthGuard]
 },
 { path: 'Tabela/aves', component: ListarAvesComponent,
 canActivate:[AuthGuard]
  },

   //rotas animal de Corte
 { path: 'atualizarCorte/:id', component: AtualizarAnimalCorteComponent},
 { path: 'Formulario/animalCorte', component: CadastarAnimalCorteComponent,
   canActivate:[AuthGuard]
 },
 { path: 'Tabela/animalCorte', component: ListarAnimalCorteComponent,
 canActivate:[AuthGuard]
  },
     //rotas animal leiteiro
 { path: 'atualizarLeiteiro/:id', component: AtualizarAnimalLeiteiroComponent},
 { path: 'Formulario/animalLeiteiro', component: CadastrarAnimalLeiteiroComponent,
   canActivate:[AuthGuard]
 },
 { path: 'Tabela/animalLeiteiro', component: ListarAnimalLeiteiroComponent,
 canActivate:[AuthGuard]
  },

    { path: '', component: HomeComponent }
   
];
export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes); 

