import { Component, OnInit } from '@angular/core';
import { AuthService } from './login/login/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
 

  mostrarMenu: boolean = false;
  //mostrarMenu: boolean = true;
  constructor(private authService: AuthService) {

  }

  ngOnInit() {
    this.authService.mostrarMenuEmitter.subscribe(
      mostrar => this.mostrarMenu = mostrar
    );
   
  }
  sair(){
    this.mostrarMenu=false;
  }
}
