import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaFuncionarioComponent } from './tabela-funcionario.component';

describe('TabelaFuncionarioComponent', () => {
  let component: TabelaFuncionarioComponent;
  let fixture: ComponentFixture<TabelaFuncionarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaFuncionarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
