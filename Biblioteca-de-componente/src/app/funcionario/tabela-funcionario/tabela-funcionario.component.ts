import { Component, OnInit } from '@angular/core';
import { FuncionarioService } from '../funcionario.service';



@Component({
  selector: 'app-tabela-funcionario',
  templateUrl: './tabela-funcionario.component.html',
  styleUrls: ['./tabela-funcionario.component.css']
})
export class TabelaFuncionarioComponent implements OnInit {
  
  funcionario: Array<any>;
  
  constructor(private funcionarioService: FuncionarioService) { }

  ngOnInit() {
    this.listar();
  
    
  }
  listar(){
  this.funcionarioService.listar().subscribe(dados => this.funcionario = dados);

  }
 
  excluir(id: number){
    this.funcionarioService.excluir(id).subscribe(()=>{
      console.log('Escluido com sucesso');
      this.listar();
      
    });
  }
}
