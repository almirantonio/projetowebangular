import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class FuncionarioService {

  
 
funcionarioUrl="http://localhost:8080/funcionario";

  constructor(private http: HttpClient) { }
  
  listar(){
  return this.http.get<any[]>(`${this.funcionarioUrl}`);
  }
  listarId(id : number){
    return this.http.get<any[]>(`http://localhost:8080/funcionario/${id}`);
    }
  
  adicionar(funcionario: any): Observable<any>{
    console.log(JSON.stringify(funcionario));
    return this.http.post('http://localhost:8080/funcionario',funcionario);
  }
  excluir(id: number){
    return this.http.delete(`http://localhost:8080/funcionario/${id}`);
  }
 
  alterar(funcionario: any): Observable<any> {
    return this.http.put(`http://localhost:8080/funcionario/${funcionario.id}`,
    funcionario);
    }
}
