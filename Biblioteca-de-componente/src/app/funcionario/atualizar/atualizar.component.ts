import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

import { FormGroup } from '@angular/forms';
import { FuncionarioService } from '../funcionario.service';

@Component({
  selector: 'app-atualizar',
  templateUrl: './atualizar.component.html',
  styleUrls: ['./atualizar.component.css']
})
export class AtualizarComponent implements OnInit {
 
id: number;
@Input()  funcionario: Array<any>;

  constructor( private router: ActivatedRoute,
    private funcionarioService: FuncionarioService) { 
    this.id= this.router.snapshot.params['id'];
    }

  ngOnInit() {
    this.listaId(this.id);
    
  }

  
  listaId(id: number){
   
    this.funcionarioService.listarId(id).subscribe(dados => this.funcionario = dados);
    
  }
 

 verificaValidTouched(campo){
  return !campo.valid && campo.touched;
}

aplicaCssErro(campo){
  return {
    'has-error': this.verificaValidTouched(campo),
    'has-feedback': this.verificaValidTouched(campo)
   
  }
  }
  atualizar(funcionario: any) {
    console.log(funcionario);
    this.funcionarioService.alterar(funcionario).subscribe(() => {
    
    
      console.log('Atualizado com sucesso');
   
    }
    );
    }
  }

