import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioFunciorarioComponent } from './formulario-funciorario.component';


describe('FormularioFunciorarioComponent', () => {
  let component: FormularioFunciorarioComponent;
  let fixture: ComponentFixture<FormularioFunciorarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioFunciorarioComponent,]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioFunciorarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
