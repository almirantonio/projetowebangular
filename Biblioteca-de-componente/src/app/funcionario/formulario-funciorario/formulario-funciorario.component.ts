import { Component, OnInit } from '@angular/core';
import {InputTextModule} from 'primeng/inputtext';
import { FormGroup } from '@angular/forms';
import { FuncionarioService } from '../funcionario.service';







@Component({
  selector: 'app-formulario-funciorario',
  templateUrl: './formulario-funciorario.component.html',
  styleUrls: ['./formulario-funciorario.component.css']
})
export class FormularioFunciorarioComponent implements OnInit {
  
funcionario: any;
  constructor(private funcionarioService: FuncionarioService) { }

  ngOnInit() {
   this.funcionario={};
  }


  onSubmit(from: FormGroup){
   console.log(this.funcionario);
   
   
    this.funcionarioService.adicionar(this.funcionario)
    .subscribe(response => this.funcionarioService.listar()
     )
  
  }



  verificaValidTouched(campo){
    return !campo.valid && campo.touched;
  }

  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }

}
