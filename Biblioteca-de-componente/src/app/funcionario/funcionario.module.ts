import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabelaFuncionarioComponent } from './tabela-funcionario/tabela-funcionario.component';
import { FormularioFunciorarioComponent } from './formulario-funciorario/formulario-funciorario.component';

import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AtualizarComponent } from './atualizar/atualizar.component';
import { FuncionarioService } from './funcionario.service';
import { AppCampoControlErroComponent } from './app-campo-control-erro/app-campo-control-erro.component';
import { CalendarModule } from 'primeng/calendar';




@NgModule({
  imports: [
    CommonModule,
    TableModule,
    RouterModule,
    FormsModule,
    CalendarModule
    
    
 
  ],
  declarations: [TabelaFuncionarioComponent,FormularioFunciorarioComponent, AtualizarComponent,AppCampoControlErroComponent],
  providers: [FuncionarioService]
})
export class FuncionarioModule { }
