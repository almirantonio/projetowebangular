import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCampoControlErroComponent } from './app-campo-control-erro.component';


describe('AppCampoControlErroComponent', () => {
  let component: AppCampoControlErroComponent;
  let fixture: ComponentFixture<AppCampoControlErroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppCampoControlErroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCampoControlErroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
