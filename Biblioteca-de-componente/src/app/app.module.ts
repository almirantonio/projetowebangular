import { HomeModule } from './home/home.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DataTableModule} from 'primeng/datatable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{ HttpClientModule}from '@angular/common/http';






import { routing } from './app.routing.module';
import { AppComponent } from './app.component';


import { HomeComponent } from './home/home/home.component';
import { FormsModule } from '@angular/forms';
import { FuncionarioModule } from './funcionario/funcionario.module';

import { AuthService } from './login/login/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { LoginModule } from './login/login.module';
import { RacaoModule } from './racao/racao.module';
import { AvesModule } from './aves/aves.module';
import { AnimalCorteModule } from './animal-corte/animal-corte.module';
import { AnimalLeiteiroModule } from './animal-leiteiro/animal-leiteiro.module';



@NgModule({
  declarations: [
    AppComponent
   
   
  ],
  imports: [
   
    BrowserModule,
   
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    routing,
    HomeModule,
    FuncionarioModule,
    LoginModule,
    RacaoModule,
    AvesModule,
    AnimalCorteModule,
    AnimalLeiteiroModule
    
    

   
  
  ],
  providers: [AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
