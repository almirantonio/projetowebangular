import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { racaoService } from '../racao.service';

@Component({
  selector: 'app-atualizar-racao',
  templateUrl: './atualizar-racao.component.html',
  styleUrls: ['./atualizar-racao.component.css']
})
export class AtualizarRacaoComponent implements OnInit {
  id: number;
  @Input()  racao: Array<any>;
  constructor( private router: ActivatedRoute,
    private racaoService: racaoService) { 
    this.id= this.router.snapshot.params['id'];
    }

  ngOnInit() {
    this.listaId(this.id);
  }


  listaId(id: number){
   
    this.racaoService.listarId(id).subscribe(dados => this.racao = dados);
    
  }

  atualizar(racao: any) {
    console.log(racao);
    this.racaoService.alterar(racao).subscribe(() => {
    
    
      console.log('Atualizado com sucesso');
   
    }
    );
    }
  verificaValidTouched(campo){
    return !campo.valid && campo.touched;
  }
  
  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }
}
