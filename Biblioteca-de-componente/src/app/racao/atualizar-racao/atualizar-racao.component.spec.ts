import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarRacaoComponent } from './atualizar-racao.component';

describe('AtualizarRacaoComponent', () => {
  let component: AtualizarRacaoComponent;
  let fixture: ComponentFixture<AtualizarRacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarRacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarRacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
