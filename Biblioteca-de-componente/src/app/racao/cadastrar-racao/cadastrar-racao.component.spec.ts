import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarRacaoComponent } from './cadastrar-racao.component';

describe('CadastrarRacaoComponent', () => {
  let component: CadastrarRacaoComponent;
  let fixture: ComponentFixture<CadastrarRacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarRacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarRacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
