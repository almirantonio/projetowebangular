import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { racaoService } from '../racao.service';
import {CalendarModule} from 'primeng/calendar';

@Component({
  selector: 'app-cadastrar-racao',
  templateUrl: './cadastrar-racao.component.html',
  styleUrls: ['./cadastrar-racao.component.css']
})
export class CadastrarRacaoComponent implements OnInit {
 
  racao: any;
  
  constructor(private racaoService: racaoService) { }

  ngOnInit() {
    this.racao={};
   
  }
  onSubmit(from: FormGroup){
    console.log(this.racao);
    
  
    
    
     this.racaoService.adicionar(this.racao)
     .subscribe(response => this.racaoService.listar()
      )
   
   }
  verificaValidTouched(campo){
    return !campo.valid && campo.touched;
  }

  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }
}
