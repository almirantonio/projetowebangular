import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarRacaoComponent } from './listar-racao.component';

describe('ListarRacaoComponent', () => {
  let component: ListarRacaoComponent;
  let fixture: ComponentFixture<ListarRacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarRacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarRacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
