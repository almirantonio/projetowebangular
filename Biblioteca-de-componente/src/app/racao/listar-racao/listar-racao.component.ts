import { Component, OnInit } from '@angular/core';
import { racaoService } from '../racao.service';

@Component({
  selector: 'app-listar-racao',
  templateUrl: './listar-racao.component.html',
  styleUrls: ['./listar-racao.component.css']
})
export class ListarRacaoComponent implements OnInit {

  racao: Array<any>;
  constructor(private racaoService: racaoService) { }

  ngOnInit() {
    this.listar();
  }
  listar(){
    this.racaoService.listar().subscribe(dados => this.racao = dados);
  
    }
   
    escluir(id: number){
      this.racaoService.escluir(id).subscribe(()=>{
        console.log('Escluido com sucesso');
        this.listar();
        
      });
}
}
