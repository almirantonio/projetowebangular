import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastrarRacaoComponent } from './cadastrar-racao/cadastrar-racao.component';
import { ListarRacaoComponent } from './listar-racao/listar-racao.component';
import { AtualizarRacaoComponent } from './atualizar-racao/atualizar-racao.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { AppCampoControlErroComponent } from './app-campo-control-erro/app-campo-control-erro.component';
import { racaoService } from './racao.service';
import { CalendarModule } from 'primeng/calendar';



@NgModule({
  imports: [
    CommonModule,
    TableModule,
    RouterModule,
    FormsModule,
    CalendarModule
   
    
  ],
  declarations: [CadastrarRacaoComponent, ListarRacaoComponent, AtualizarRacaoComponent,
    AppCampoControlErroComponent],
    providers: [racaoService]
})
export class RacaoModule { }
