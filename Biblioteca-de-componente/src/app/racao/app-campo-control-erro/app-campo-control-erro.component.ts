import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-app-campo-control-erro',
  templateUrl: './app-campo-control-erro.component.html',
  styleUrls: ['./app-campo-control-erro.component.css']
})
export class AppCampoControlErroComponent implements OnInit {
 
  @Input() msgErro: string;
  @Input() mostrarErro: boolean;
  
  constructor() { }

  ngOnInit() {
  }

}
