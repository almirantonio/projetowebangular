import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class racaoService {

  


  constructor(private http: HttpClient) { }
  
  listar(){
  return this.http.get<any[]>('http://localhost:8080/racao');
  }
  listarId(id : number){
    return this.http.get<any[]>(`http://localhost:8080/racao/${id}`);
    }
  
  adicionar(racao: any): Observable<any>{
    console.log(JSON.stringify(racao));
    return this.http.post('http://localhost:8080/racao',racao);
  }
  escluir(id: number){
    return this.http.delete(`http://localhost:8080/racao/${id}`);
  }
 
  alterar(racao: any): Observable<any> {
    return this.http.put(`http://localhost:8080/racao/${racao.id}`,
    racao);
    }
}
