import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarAvesComponent } from './cadastrar-aves.component';

describe('CadastrarAvesComponent', () => {
  let component: CadastrarAvesComponent;
  let fixture: ComponentFixture<CadastrarAvesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarAvesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarAvesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
