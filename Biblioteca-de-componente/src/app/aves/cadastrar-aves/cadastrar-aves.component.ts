
import { Component, OnInit, Input } from '@angular/core';
import {CalendarModule} from 'primeng/calendar';
import { FormGroup } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AvesService } from '../Aves.service';

@Component({
  selector: 'app-cadastrar-aves',
  templateUrl: './cadastrar-aves.component.html',
  styleUrls: ['./cadastrar-aves.component.css']
})
export class CadastrarAvesComponent implements OnInit {
  data_vacinacao: Date;
  data_nascimento: Date;
  aves: any;
 
  constructor(private avesService: AvesService) { }

  ngOnInit() {
   this.aves={};
   
   
  }


  onSubmit(from: FormGroup){
    
   this.aves.data_vacinacao= this.data_vacinacao.toLocaleDateString();
   this.aves.data_nascimento= this.data_nascimento.toLocaleDateString();
   
    console.log(this.aves);
   
   
    this.avesService.adicionar(this.aves)
    .subscribe(response => this.avesService.listar()
     )
  
  }



  verificaValidTouched(campo){
    return !campo.valid && campo.touched;
  }

  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }

 
  
}
