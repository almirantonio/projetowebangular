import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastrarAvesComponent } from './cadastrar-aves/cadastrar-aves.component';
import { AtualizarAvesComponent } from './atualizar-aves/atualizar-aves.component';
import { ListarAvesComponent } from './listar-aves/listar-aves.component';
import {CalendarModule} from 'primeng/calendar';
import { AppCampoControlErroComponent } from './app-campo-control-erro/app-campo-control-erro.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { AvesService } from './Aves.service';


@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    TableModule,
    RouterModule,
    FormsModule
    
  ],
  declarations: [CadastrarAvesComponent, AtualizarAvesComponent, ListarAvesComponent,AppCampoControlErroComponent],
  providers: [ AvesService]
})
export class AvesModule { }
