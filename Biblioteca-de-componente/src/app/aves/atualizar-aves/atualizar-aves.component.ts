import { Component, OnInit, Input } from '@angular/core';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AvesService } from '../Aves.service';

@Component({
  selector: 'app-atualizar-aves',
  templateUrl: './atualizar-aves.component.html',
  styleUrls: ['./atualizar-aves.component.css']
})
export class AtualizarAvesComponent implements OnInit {
  data_vacinacao: Date;
  data_nascimento: Date;
 

  id: number;
@Input()  aves: Array<any>;
Data:boolean=false;
  constructor(private router: ActivatedRoute,
    private avesService: AvesService) { 
    this.id= this.router.snapshot.params['id'];
    
  }

  ngOnInit() {
    this.listaId(this.id);
   
  }
  listaId(id: number){
   
    this.avesService.listarId(id).subscribe(dados => this.aves = dados);

    
  }

  
  atualizar(aves: any) {
    aves.data_vacinacao= this.data_vacinacao.toLocaleDateString();
    aves.data_nascimento= this.data_nascimento.toLocaleDateString();

    console.log(aves);
 this.avesService.alterar(aves).subscribe(() => {
     console.log('Atualizado com sucesso');
    
   
    }
    );
    }

    verificaValidTouched(campo){
      return !campo.valid && campo.touched;
    }
  aplicaCssErro(campo){
    return {
      'has-error': this.verificaValidTouched(campo),
      'has-feedback': this.verificaValidTouched(campo)
     
    }
  }

  dataCondicao(campo){
    if( campo.touched){
     this.Data=true;
    }
  }
}
