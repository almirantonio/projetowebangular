import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAvesComponent } from './atualizar-aves.component';

describe('AtualizarAvesComponent', () => {
  let component: AtualizarAvesComponent;
  let fixture: ComponentFixture<AtualizarAvesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarAvesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAvesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
