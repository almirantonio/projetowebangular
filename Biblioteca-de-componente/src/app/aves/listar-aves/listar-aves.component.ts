import { Component, OnInit } from '@angular/core';
import { AvesService } from '../Aves.service';

@Component({
  selector: 'app-listar-aves',
  templateUrl: './listar-aves.component.html',
  styleUrls: ['./listar-aves.component.css']
})
export class ListarAvesComponent implements OnInit {
  aves: Array<any>;
  constructor(private avesService: AvesService) { }

  ngOnInit() {
    this.listar();
  }
  listar(){
    this.avesService.listar().subscribe(dados => this.aves = dados);
  
    }
   
    excluir(id: number){
      this.avesService.excluir(id).subscribe(()=>{
        console.log('Escluido com sucesso');
        this.listar();
        
      });
    }

}
