import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AvesService {

  

  constructor(private http: HttpClient) { }
  
  listar(){
  return this.http.get<any[]>('http://localhost:8080/animal/aves');
  }
  listarId(id : number){
    return this.http.get<any[]>(`http://localhost:8080/animal/aves/${id}`);
    }
  
  adicionar(aves: any): Observable<any>{
    console.log(JSON.stringify(aves));
    return this.http.post('http://localhost:8080/animal/aves',aves);
  }
  excluir(id: number){
    return this.http.delete(`http://localhost:8080/animal/aves/${id}`);
  }
 
  alterar(aves: any): Observable<any> {
    return this.http.put(`http://localhost:8080/animal/aves/${aves.id}`,
    aves);
    }
}
