import { Usuario } from './usuario';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  private usuarioAutenticado: boolean = false;

  mostrarMenuEmitter = new EventEmitter<boolean>();

  constructor(private router: Router) { }

  fazerLogin(usuario: Usuario){

    if (usuario.email === 'a' && usuario.senha === '1') {

      this.usuarioAutenticado = true;

      this.mostrarMenuEmitter.emit(true);

      this.router.navigate(['/']);
      console.log(this.usuarioAutenticado);
      console.log();
      
    } else {
      this.usuarioAutenticado = false;

      this.mostrarMenuEmitter.emit(false);
      console.log(this.usuarioAutenticado);
    }
  }

  usuarioEstaAutenticado(){
    return this.usuarioAutenticado;
  }

}
